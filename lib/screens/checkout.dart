import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mrboim_app/components/alert.dart';
import 'package:mrboim_app/components/line.dart';
import 'package:mrboim_app/components/order_item.dart';
import 'package:mrboim_app/components/order_note.dart';
import 'package:mrboim_app/containers/schedule_datetime_bottom_sheet.dart';
import 'package:mrboim_app/containers/schedule_option.dart';
import 'package:mrboim_app/controllers/schedule_controller.dart';

class CheckoutScreen extends StatefulWidget {
  const CheckoutScreen({super.key});

  @override
  State<CheckoutScreen> createState() => _CheckoutScreen();
}

class _CheckoutScreen extends State<CheckoutScreen> {
  String activeDeliveryDay = 'reguler';

  void onChangeDeliveryDay(String deliveryDay) {
    setState(() {
      activeDeliveryDay = deliveryDay;
    });
  }

  @override
  Widget build(BuildContext context) {
    ScheduleController controller = Get.find();

    return SafeArea(
      child: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            bottomOpacity: 0,
            leading: IconButton(
              icon: const Icon(Icons.arrow_back_ios, color: Colors.black),
              onPressed: () => Get.back(),
            ),
            title: const Text("Checkout",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22)),
            centerTitle: true,
          ),
          body: ListView(
            children: [
              Obx(() => Visibility(
                  visible: !controller.isSetupSchedule.value,
                  child: Container(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                    child: const Alert(
                      type: AlertType.info,
                      label: "Klik \"Ganti\" untuk atur waktu penjemputan",
                      icon: Icons.info_rounded,
                    ),
                  ))),
              Obx(() => Container(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                    child: ScheduleOption(
                        isSetupSchedule: controller.isSetupSchedule.value,
                        currentDateTime: controller.dateTime.value,
                        onTap: () {
                          showModalBottomSheet(
                              context: context,
                              backgroundColor: Colors.white,
                              builder: (context) => ScheduleDateTimeBottomSheet(
                                    controller: controller,
                                  ));
                        }),
                  )),
              const SizedBox(height: 6),
              InkWell(
                splashColor: Colors.transparent,
                onTap: () => Get.toNamed("/location-setup"),
                child: Container(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text(
                                  "Rumah",
                                  style: TextStyle(
                                      fontSize: 16,
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold),
                                ),
                                const Text(
                                  "Rangkapan jaya, Pancoranmas No.104 asdasdas dasdasdada",
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                    color: Colors.grey,
                                  ),
                                ),
                                const SizedBox(height: 8),
                                Container(
                                  color: Colors.grey.withOpacity(0.1),
                                  padding: EdgeInsets.all(6),
                                  child: Row(
                                    children: [
                                      Icon(Icons.comment,
                                          color: Colors.black.withOpacity(0.6)),
                                      SizedBox(width: 8),
                                      Flexible(
                                          child: Text(
                                              "Lokasi kamu jauh dari titik bang boim nih, kira-kira lebih dari 10km",
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                  color: Colors.black
                                                      .withOpacity(0.6))))
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.only(left: 16, right: 8),
                            child: const Icon(Icons.arrow_forward_ios_rounded,
                                color: Colors.black87, size: 20),
                          ),
                        ],
                      ),
                      const SizedBox(height: 16),
                    ],
                  ),
                ),
              ),
              Line(),
              Container(
                padding: const EdgeInsets.only(left: 16, right: 16, top: 8),
                child: const Text("Detail Pembayaran",
                    style:
                        TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
              ),
              Container(
                margin: const EdgeInsets.all(16),
                padding: const EdgeInsets.all(16),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(8),
                    border: Border.all(color: Colors.grey.withOpacity(0.4))),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const OrderItem(
                      margin: EdgeInsets.only(bottom: 16),
                      itemName: "Kaos",
                      itemQty: 3,
                      itemPrice: 2800,
                    ),
                    const OrderItem(
                      margin: EdgeInsets.only(bottom: 16),
                      itemName: "Dress",
                      itemQty: 1,
                      itemPrice: 2800,
                    ),
                    const OrderItem(
                      margin: EdgeInsets.only(bottom: 16),
                      itemName: "Celana Panjang",
                      itemQty: 1,
                      itemPrice: 2800,
                    ),
                    const OrderItem(
                      margin: EdgeInsets.only(bottom: 16),
                      itemName: "Kaus kaki",
                      itemQty: 2,
                      itemPrice: 2800,
                    ),
                    const OrderItem(
                      margin: EdgeInsets.only(bottom: 16),
                      itemName: "Rok",
                      itemQty: 1,
                      itemPrice: 2800,
                    ),
                    Container(
                      margin: const EdgeInsets.only(bottom: 8),
                      child: const Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("Biaya Antar-Jemput",
                              style: TextStyle(fontSize: 16)),
                          Text("Rp20.000", style: TextStyle(fontSize: 16))
                        ],
                      ),
                    ),
                    const Visibility(
                        child: OrderNote(
                            maxLines: 2,
                            note:
                                "Lokasi kamu jauh dari titik bang boim nih, kira-kira lebih dari 10km")),
                  ],
                ),
              ),
              Container(
                padding: const EdgeInsets.only(left: 16, right: 16, bottom: 32),
                child: const Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Catatan :"),
                    Text(
                        "Untuk cuci kiloan estimasi total kiloan akan ditimbang ulang oleh bang boim yang datang ke tempatmu ya.")
                  ],
                ),
              )
            ],
          ),
          bottomNavigationBar: Container(
            padding: const EdgeInsets.all(16),
            height: 90,
            decoration: BoxDecoration(color: Colors.white, boxShadow: [
              BoxShadow(
                  color: Colors.grey.withOpacity(0.4),
                  blurRadius: 10,
                  spreadRadius: 0),
            ]),
            child: Obx(() => InkWell(
                  onTap: controller.isSetupSchedule.value
                      ? () => Get.toNamed("/waiting-room")
                      : () {},
                  child: Container(
                    decoration: BoxDecoration(
                        color: controller.isSetupSchedule.value
                            ? const Color(0xff00AA13)
                            : Colors.grey.withOpacity(0.4),
                        borderRadius: BorderRadius.circular(100)),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          margin: const EdgeInsets.only(left: 24),
                          child: Text("Bayar Sekarang",
                              style: TextStyle(
                                fontSize: 16,
                                color: controller.isSetupSchedule.value
                                    ? Colors.white
                                    : Colors.black45,
                                fontWeight: FontWeight.bold,
                              )),
                        ),
                        Container(
                          margin: const EdgeInsets.only(right: 24),
                          child: const Text("Rp10.000",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold)),
                        )
                      ],
                    ),
                  ),
                )),
          )),
    );
  }
}
