import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class WaitingRoomScreen extends StatelessWidget {
  const WaitingRoomScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const Text("Tunggu ya! Pesanan kamu akan segera boim proses",
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.grey, fontSize: 26)),
          Lottie.asset('images/lotties/anim-waiting-with-bike.json'),
        ],
      ),
    ));
  }
}
