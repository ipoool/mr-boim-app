import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mrboim_app/commons/constant.dart';
import 'package:mrboim_app/components/card/card_service.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreen();
}

class _HomeScreen extends State<HomeScreen> {
  int _selectedIndex = 0;

  void _onTabBottomNavvigation(int currentIndex) {
    setState(() {
      _selectedIndex = currentIndex;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: ListView(
          children: [
            Container(
              color: Colors.black,
              height: 200,
              child: Center(
                child: Text("Slide Banner"),
              ),
            ),
            Container(
              padding: const EdgeInsets.all(16),
              decoration: BoxDecoration(
                color: Colors.grey.withOpacity(0.1),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    margin: const EdgeInsets.only(bottom: 16),
                    child: Text(
                      "Layanan",
                      style: TextStyle(
                          color: Colors.black87.withOpacity(0.6),
                          fontWeight: FontWeight.bold,
                          fontSize: 18),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(bottom: 24),
                    height: 100,
                    child: ListView(
                      scrollDirection: Axis.horizontal,
                      children: [
                        CardService(
                            image: serviceList["wash_fold"]!["image"],
                            title: serviceList["wash_fold"]["title"],
                            estDuration: serviceList["wash_fold"]["duration"],
                            onClick: () {
                              Get.toNamed("/order", arguments: "wash_fold");
                            }),
                        CardService(
                          image: serviceList["iron_fold"]["image"],
                          title: serviceList["iron_fold"]["title"],
                          estDuration: serviceList["iron_fold"]["duration"],
                          onClick: () {
                            Get.toNamed("/order", arguments: "iron_fold");
                          },
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.only(bottom: 16),
              child: const Text(
                "Promo Terbaru",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
              ),
            ),
          ],
        ),
        bottomNavigationBar: Theme(
          data: ThemeData(
            splashColor: Colors.transparent,
            // highlightColor: Colors.transparent,
          ),
          child: BottomNavigationBar(
              showSelectedLabels: false,
              showUnselectedLabels: false,
              currentIndex: _selectedIndex,
              iconSize: 40,
              onTap: _onTabBottomNavvigation,
              selectedItemColor: Colors.blueAccent,
              unselectedItemColor: Colors.black38,
              items: const <BottomNavigationBarItem>[
                BottomNavigationBarItem(
                  icon: Icon(Icons.home),
                  label: 'Home',
                ),
                BottomNavigationBarItem(
                  icon: Icon(
                    Icons.shopping_basket,
                  ),
                  label: 'Order',
                ),
                BottomNavigationBarItem(
                  icon: Icon(
                    Icons.local_activity,
                  ),
                  label: 'Promotion',
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.person),
                  label: 'Profile',
                )
              ]),
        ),
      ),
    );
  }
}
