import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mrboim_app/commons/app.dart';
import 'package:mrboim_app/commons/constant.dart';
import 'package:mrboim_app/components/button/button_order_wash.dart';
import 'package:mrboim_app/components/line.dart';
import 'package:mrboim_app/containers/extra_product_bottom_sheet.dart';
import 'package:mrboim_app/containers/product_kilo.dart';
import 'package:mrboim_app/containers/product_piece.dart';
import 'package:mrboim_app/containers/wash_type.dart';
import 'package:mrboim_app/containers/wash_type_bottom_sheet.dart';
import 'package:mrboim_app/controllers/order_controller.dart';

class OrderServiceScreen extends StatefulWidget {
  const OrderServiceScreen({super.key});

  @override
  State<OrderServiceScreen> createState() => _OrderServiceScreenState();
}

class _OrderServiceScreenState extends State<OrderServiceScreen> {
  String? service = Get.arguments;

  @override
  Widget build(BuildContext context) {
    OrderController controller = Get.find();
    return SafeArea(
      child: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            bottomOpacity: 0,
            leading: IconButton(
              icon: const Icon(Icons.arrow_back_ios, color: Colors.black),
              onPressed: () => Get.back(),
            ),
            title: Text(serviceList[service]["title"],
                style:
                    const TextStyle(fontWeight: FontWeight.bold, fontSize: 22)),
            centerTitle: true,
          ),
          body: ListView(
            children: [
              const SizedBox(height: 8),
              Obx(() => WashType(
                    active: controller.washType.value,
                    onTap: () {
                      showModalBottomSheet(
                          context: context,
                          backgroundColor: Colors.white,
                          builder: (context) =>
                              WashTypeBottomSheet(controller: controller));
                    },
                  )),
              const Line(),
              Obx(() => (controller.washType.value == "satuan")
                  ? ProductPiece(controller: controller)
                  : ProductKilo(controller: controller)),
              Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Mau cepet beres?",
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.bold)),
                        SizedBox(height: 4),
                        Text(
                          "Cuci express sesuai kebutuhanmu!",
                          style: TextStyle(color: Colors.grey),
                        )
                      ],
                    ),
                    InkWell(
                      onTap: () => {
                        showModalBottomSheet(
                            context: context,
                            backgroundColor: Colors.white,
                            builder: (context) =>
                                const ExtraProductBottomSheet())
                      },
                      child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(100),
                            border: Border.all(
                              color: const Color(blueButtonActionColor),
                            )),
                        padding: const EdgeInsets.symmetric(
                            horizontal: 16, vertical: 10),
                        child: const Text("Tambah",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Color(blueButtonActionColor))),
                      ),
                    )
                  ],
                ),
              ),
              Divider(color: Colors.grey.withOpacity(0.4)),
              const SizedBox(height: 90)
            ],
          ),
          floatingActionButtonLocation:
              FloatingActionButtonLocation.centerFloat,
          floatingActionButton: Obx(() => ButtonOrderWash(
                activeWashType: controller.washType.value,
                total: controller.washType.value == "satuan"
                    ? controller.totalQty.value
                    : controller.totalKilo.value,
                totalPrice: 147000,
                onTap: () {
                  Get.toNamed("/checkout", arguments: service);
                },
              ))),
    );
  }
}
