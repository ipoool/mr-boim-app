import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mrboim_app/containers/location_bottom_sheet.dart';

class LocationSetupScreen extends StatelessWidget {
  const LocationSetupScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: Stack(
        children: [
          Container(
            color: Colors.grey.withOpacity(0.4),
            height: double.infinity,
            child: const Center(
              child: Text("Google map will be here"),
            ),
          ),
          Positioned(
              top: 16,
              left: 16,
              child: Container(
                decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.4),
                        blurRadius: 3,
                        spreadRadius: 2,
                      )
                    ],
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(100)),
                child: IconButton(
                  highlightColor: Colors.transparent,
                  icon: const Icon(Icons.arrow_back_ios_new,
                      color: Colors.black87),
                  onPressed: () => Get.back(),
                ),
              )),
        ],
      ),
      bottomSheet: const LocationBottomSheet(),
    ));
  }
}
