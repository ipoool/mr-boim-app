class ItemModel {
  String key;
  String title;
  String image;

  ItemModel(this.key, this.title, this.image);
}
