const int blueButtonActionColor = 0xff00AED6;
const int blueTextColor = 0xff6cb2d8;
const int blueBackgroundColor = 0xffE9F4F9;

const int defaultTotalQty = 0;
const int defaultTotalKilo = 5;
const double defaultLimitKilo = 20;
