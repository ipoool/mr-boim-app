import 'package:mrboim_app/models/item_model.dart';

const Map serviceList = {
  'wash_fold': {
    'image': 'images/washing-machine.png',
    'title': 'Cuci',
    'duration': 'Min. 3kg',
    'description': 'Kamu bisa cuci apapun jenis pakaian dengan Rp10.000/kg',
  },
  'iron_fold': {
    'image': 'images/iron.png',
    'title': 'Gosok',
    'duration': 'Min. 3kg',
    'description': 'Kamu bisa cuci apapun jenis pakaian dengan Rp10.000/kg',
  },
  'dry_clean': {
    'image': 'images/dry-clean.png',
    'title': 'Dry Clean',
    'duration': 'Min. 3 hari',
    'description': 'Kamu bisa cuci apapun jenis pakaian dengan Rp10.000/kg',
  },
  'premium_wash': {
    'image': 'images/premium-wash.png',
    'title': 'Cuci Premium',
    'duration': 'Min. 2 hari',
    'description': 'Kamu bisa cuci apapun jenis pakaian dengan Rp10.000/kg',
  },
};

List<ItemModel> listItems = <ItemModel>[
  ItemModel('tshirt', 'Kaos', 'images/shirt-color.png'),
  ItemModel('outwear', 'Pakaian Luar', 'images/hoodie.png'),
  ItemModel('dress', 'Dress', 'images/dress.png'),
  ItemModel('shortpant', 'Celana Pendek', 'images/short.png'),
  ItemModel('longpant', 'Celana Panjang', 'images/long-pant.png'),
  ItemModel('skirt', 'Rok', 'images/skirt.png'),
  // ItemModel('underwear', 'Underwear', 'images/underwear.png'),
  ItemModel('socks', 'Kaus Kaki', 'images/socks.png'),
];

const Map washTypeList = {
  "satuan": {
    "title": "Cuci Satuan",
    "subtitle": "Promo untuk 2 item",
    "image": "images/tshirt-fold.png"
  },
  "kilo": {
    "title": "Cuci Kilo",
    "subtitle": "Promo untuk lebih dari 10kg",
    "image": "images/weight.png"
  }
};

const Map<int, String> dayNameID = {
  1: "Senin",
  2: "Selasa",
  3: "Rabu",
  4: "Kamis",
  5: "Jumat",
  6: "Sabtu",
  7: "Minggu"
};

const Map<int, String> monthNameID = {
  1: "Januari",
  2: "Febuari",
  3: "Maret",
  4: "April",
  5: "Mei",
  6: "Juni",
  7: "Juli",
  8: "Agustus",
  9: "September",
  10: "Oktober",
  11: "November",
  12: "Desember"
};

const Map<int, String> monthNameIDmin = {
  1: "Jan",
  2: "Feb",
  3: "Mar",
  4: "Apr",
  5: "Mei",
  6: "Jun",
  7: "Jul",
  8: "Agu",
  9: "Sep",
  10: "Okt",
  11: "Nov",
  12: "Des"
};
