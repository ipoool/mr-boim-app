import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

DateTime dateTimeNow() {
  return DateTime.now();
}

List<String> createTimeSlot(
    Duration startTime, Duration endTime, BuildContext context,
    {Duration step = const Duration(minutes: 30)} // Gap between interval
    ) {
  var timeSlot = <String>[];
  var hourStartTime = startTime.inHours;
  var minuteStartTime = startTime.inMinutes.remainder(60);
  var hourEndTime = endTime.inHours;
  var minuteEndTime = endTime.inMinutes.remainder(60);

  do {
    timeSlot.add(TimeOfDay(hour: hourStartTime, minute: minuteStartTime)
        .format(context));
    minuteStartTime += step.inMinutes;
    while (minuteStartTime >= 60) {
      minuteStartTime -= 60;
      hourStartTime++;
    }
  } while (hourStartTime < hourEndTime ||
      (hourStartTime == hourEndTime && minuteStartTime <= minuteEndTime));

  return timeSlot;
}

List<DateTime> getListDateBeforeDateNow() {
  DateTime now = DateTime.now().add(const Duration(days: -1));
  DateTime januaryFirst = DateTime(now.year, 1, 1);
  List<DateTime> dateList = [];

  for (int i = 0; i <= now.difference(januaryFirst).inDays; i++) {
    DateTime date = januaryFirst.add(Duration(days: i));
    dateList.add(date);
  }

  return dateList;
}

TimeOfDay stringToTimeOfDay(String tod) {
  final format = DateFormat("h:mm a");
  return TimeOfDay.fromDateTime(format.parse(tod));
}
