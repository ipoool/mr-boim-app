import 'package:money_formatter/money_formatter.dart';

String formatRupiah(double amount) {
  MoneyFormatter fmf = MoneyFormatter(
      amount: amount,
      settings: MoneyFormatterSettings(
          symbol: 'Rp',
          thousandSeparator: '.',
          decimalSeparator: ',',
          symbolAndNumberSeparator: '',
          fractionDigits: 0));

  return fmf.output.symbolOnLeft;
}
