import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mrboim_app/controllers/order_controller.dart';
import 'package:mrboim_app/controllers/schedule_controller.dart';
import 'package:mrboim_app/screens/checkout.dart';
import 'package:mrboim_app/screens/home.dart';
import 'package:mrboim_app/screens/location_setup.dart';
import 'package:mrboim_app/screens/order_service.dart';
import 'package:mrboim_app/screens/waiting_room.dart';

class InitDependency implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => OrderController(), fenix: true);
    Get.lazyPut(() => ScheduleController(), fenix: true);
    print("Initialize dependencies controller");
  }
}

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    return GetMaterialApp(
      initialBinding: InitDependency(),
      title: 'Mr.Boim - Cloud Laundry Indonesia',
      theme: ThemeData(
          appBarTheme: const AppBarTheme(
            elevation: 0,
            backgroundColor: Colors.transparent,
          ),
          colorScheme: ColorScheme.fromSeed(seedColor: Colors.blue),
          useMaterial3: true,
          textTheme: GoogleFonts.poppinsTextTheme(textTheme).copyWith(
              bodyMedium:
                  GoogleFonts.poppins(textStyle: textTheme.bodyMedium))),
      home: const HomeScreen(),
      getPages: [
        GetPage(name: "/", page: () => const HomeScreen()),
        GetPage(name: "/order", page: () => const OrderServiceScreen()),
        GetPage(name: "/checkout", page: () => const CheckoutScreen()),
        GetPage(
            name: "/location-setup", page: () => const LocationSetupScreen()),
        GetPage(
          name: "/waiting-room",
          page: () => const WaitingRoomScreen(),
        )
      ],
    );
  }
}
