import 'package:flutter/material.dart';
import 'package:mrboim_app/helpers/format.dart';

class ServiceTypeOption extends StatelessWidget {
  final String title;
  final double price;
  const ServiceTypeOption({super.key, required this.title, this.price = 0});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Radio(
          value: "1",
          groupValue: "extra",
          onChanged: (value) {
            print(value);
          },
        ),
        Text(title,
            style: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
        const Spacer(),
        Text(formatRupiah(price),
            style: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
      ],
    );
  }
}
