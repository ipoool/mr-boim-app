import 'package:flutter/material.dart';
import 'package:mrboim_app/commons/app.dart';

class WashTypeItem extends StatelessWidget {
  final String image;
  final String title;
  final String subtitle;
  final bool isActive;
  final VoidCallback? onTap;
  const WashTypeItem(
      {super.key,
      required this.image,
      required this.title,
      this.subtitle = '',
      this.isActive = false,
      this.onTap});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        padding: const EdgeInsets.all(16),
        decoration: BoxDecoration(
            color: isActive
                ? const Color(blueButtonActionColor).withOpacity(0.8)
                : Colors.white,
            borderRadius: BorderRadius.circular(16),
            border: Border.all(
                color: isActive
                    ? const Color(blueButtonActionColor)
                    : Colors.grey.withOpacity(0.4))),
        child: Row(
          children: [
            SizedBox(
              width: 60,
              child: Image.asset(image),
            ),
            const SizedBox(width: 16),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(title,
                    style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                        color: isActive ? Colors.white : Colors.black87)),
                Text(subtitle,
                    style:
                        TextStyle(color: isActive ? Colors.white : Colors.grey))
              ],
            )
          ],
        ),
      ),
    );
  }
}
