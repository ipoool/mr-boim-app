import 'package:flutter/material.dart';

class OrderNote extends StatelessWidget {
  final String note;
  final int maxLines;
  final TextOverflow? overflow;
  final Color? color;
  const OrderNote(
      {super.key,
      required this.note,
      this.maxLines = 1,
      this.overflow,
      this.color});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.all(8),
      decoration: BoxDecoration(
          color: color,
          border: Border(
              left: BorderSide(width: 5, color: Colors.grey.withOpacity(0.3)))),
      margin: const EdgeInsets.only(bottom: 8),
      child: Text(note,
          maxLines: maxLines,
          overflow: overflow,
          style: const TextStyle(color: Colors.black54)),
    );
  }
}
