import 'package:flutter/material.dart';
import 'package:mrboim_app/helpers/format.dart';

class OrderItem extends StatelessWidget {
  final EdgeInsetsGeometry? margin;
  final String itemName;
  final int itemQty;
  final double itemPrice;
  const OrderItem(
      {super.key,
      this.margin,
      required this.itemName,
      required this.itemQty,
      this.itemPrice = 0});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(itemName, style: TextStyle(fontSize: 16)),
              SizedBox(width: 10),
              Text("x${itemQty.toString()}",
                  style: TextStyle(
                    color: Colors.grey,
                  ))
            ],
          ),
          Text(formatRupiah(itemPrice * itemQty),
              style: TextStyle(fontSize: 16))
        ],
      ),
    );
  }
}
