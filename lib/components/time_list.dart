import 'package:flutter/material.dart';
import 'package:mrboim_app/commons/app.dart';
import 'package:mrboim_app/helpers/date_time.dart';

class TimeList extends StatelessWidget {
  final DateTime dateTime;
  final TimeOfDay currentTime;
  final Function(TimeOfDay value) onChange;
  const TimeList({
    super.key,
    required this.dateTime,
    required this.onChange,
    required this.currentTime,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 50,
      child: ListView(
        shrinkWrap: false,
        scrollDirection: Axis.horizontal,
        children: [
          ...createTimeSlot(Duration(hours: dateTime.hour + 1, minutes: 0),
                  const Duration(hours: 23, minutes: 0), context)
              .map((item) {
            TimeOfDay timeItem = stringToTimeOfDay(item);
            return Row(
              children: [
                InkWell(
                  splashColor: Colors.transparent,
                  onTap: () {
                    TimeOfDay time = stringToTimeOfDay(item);
                    onChange(time);
                  },
                  child: Container(
                    padding: const EdgeInsets.all(16),
                    decoration: BoxDecoration(
                        color: currentTime?.hour == timeItem.hour &&
                                currentTime?.minute == timeItem.minute
                            ? const Color(blueButtonActionColor)
                            : Colors.white,
                        border: Border.all(
                            color: currentTime?.hour == timeItem.hour &&
                                    currentTime?.minute == timeItem.minute
                                ? const Color(blueButtonActionColor)
                                : Colors.grey),
                        borderRadius: BorderRadius.circular(8)),
                    child: Text(item,
                        style: TextStyle(
                            color: currentTime?.hour == timeItem.hour &&
                                    currentTime?.minute == timeItem.minute
                                ? Colors.white
                                : Colors.black)),
                  ),
                ),
                const SizedBox(width: 16)
              ],
            );
          })
        ],
      ),
    );
  }
}
