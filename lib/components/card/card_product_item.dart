import 'package:flutter/material.dart';
import 'package:mrboim_app/commons/app.dart';
import 'package:mrboim_app/helpers/format.dart';

class CardProductItem extends StatelessWidget {
  final String title;
  final double price;
  final String image;
  final int qty;
  final bool isDisabledLine;
  final VoidCallback? removePressed;
  final VoidCallback? addPressed;
  final VoidCallback? serviceTypePressed;
  const CardProductItem({
    super.key,
    required this.title,
    required this.price,
    required this.image,
    this.removePressed,
    this.addPressed,
    this.serviceTypePressed,
    this.qty = 0,
    this.isDisabledLine = false,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
            padding: const EdgeInsets.symmetric(vertical: 8),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Row(
                  children: [
                    Container(
                      padding: const EdgeInsets.all(8),
                      width: 90,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          border:
                              Border.all(color: Colors.grey.withOpacity(0.4))),
                      child: Image.asset(image),
                    ),
                    const SizedBox(width: 16),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(title,
                            style: const TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold)),
                        Text(formatRupiah(price),
                            style: const TextStyle(
                                fontSize: 16, color: Colors.black54)),
                        const SizedBox(height: 8),
                        InkWell(
                          splashColor: Colors.transparent,
                          onTap: serviceTypePressed,
                          child: Container(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 8, vertical: 4),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(8),
                                color: const Color(blueBackgroundColor)),
                            child: const Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text("Cuci aja",
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold)),
                                Icon(Icons.keyboard_arrow_down_rounded)
                              ],
                            ),
                          ),
                        )
                      ],
                    )
                  ],
                ),
                Row(
                  children: [
                    Ink(
                      decoration: BoxDecoration(
                        border: Border.all(
                            color: qty == 0
                                ? Colors.grey
                                : const Color(blueButtonActionColor)),
                        shape: BoxShape.circle,
                      ),
                      child: InkWell(
                        borderRadius: BorderRadius.circular(100),
                        onTap: qty == 0 ? null : removePressed,
                        child: Icon(Icons.remove,
                            color: qty == 0
                                ? Colors.grey
                                : const Color(blueButtonActionColor)),
                      ),
                    ),
                    Container(
                      width: 50,
                      padding: const EdgeInsets.only(left: 16, right: 16),
                      child: Center(
                        child: Text(qty.toString(),
                            style: const TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold)),
                      ),
                    ),
                    Ink(
                      decoration: BoxDecoration(
                        border: Border.all(
                            color: const Color(blueButtonActionColor)),
                        shape: BoxShape.circle,
                      ),
                      child: InkWell(
                        borderRadius: BorderRadius.circular(100),
                        onTap: addPressed,
                        child: const Icon(
                          Icons.add,
                          color: Color(blueButtonActionColor),
                        ),
                      ),
                    )
                  ],
                )
              ],
            )),
        Divider(
          color: Colors.grey.withOpacity(0.4),
        )
      ],
    );
  }
}
