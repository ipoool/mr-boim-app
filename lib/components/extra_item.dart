import 'package:flutter/material.dart';

class ExtraItem extends StatelessWidget {
  final String title;
  final String subtitle;
  const ExtraItem({super.key, required this.title, this.subtitle = ''});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          padding: const EdgeInsets.symmetric(vertical: 8),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(title,
                      style: const TextStyle(
                          fontSize: 16, fontWeight: FontWeight.bold)),
                  if (subtitle != "") const SizedBox(height: 2),
                  if (subtitle != "")
                    Text(subtitle, style: const TextStyle(color: Colors.grey))
                ],
              ),
              Radio(
                value: "1",
                groupValue: "extra",
                onChanged: (value) {
                  print(value);
                },
              )
            ],
          ),
        ),
        Divider(color: Colors.grey.withOpacity(0.4))
      ],
    );
  }
}
