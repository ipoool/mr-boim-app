import 'package:flutter/material.dart';

enum AlertType { info, warning, error, success }

class Alert extends StatelessWidget {
  final AlertType type;
  final IconData? icon;
  final String label;
  const Alert({super.key, required this.type, this.icon, this.label = ''});

  @override
  Widget build(BuildContext context) {
    Color textColor = Colors.white;
    Color bgColor = Colors.white;

    if (type == AlertType.success) {
      textColor = const Color(0xff04A914);
      bgColor = const Color(0xff04A914).withOpacity(0.2);
    } else if (type == AlertType.info) {
      textColor = const Color(0xff00AED6);
      bgColor = const Color(0xff00AED6).withOpacity(0.2);
    } else if (type == AlertType.error) {
      textColor = const Color(0xffEE2737);
      bgColor = const Color(0xffEE2737).withOpacity(0.2);
    } else if (type == AlertType.warning) {
      textColor = const Color(0xffF06400);
      bgColor = const Color(0xffF06400).withOpacity(0.2);
    }

    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 8),
      decoration: BoxDecoration(
          color: bgColor, borderRadius: BorderRadius.circular(100)),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          if (icon != null) Icon(icon, size: 20, color: textColor),
          if (icon != null) const SizedBox(width: 8),
          Text(label,
              style: TextStyle(color: textColor, fontWeight: FontWeight.bold))
        ],
      ),
    );
  }
}
