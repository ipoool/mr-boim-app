import 'package:flutter/material.dart';

class Line extends StatelessWidget {
  const Line({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 8,
      decoration: BoxDecoration(
          color: Colors.grey.withOpacity(0.2),
          border: Border(top: BorderSide(color: Colors.grey.withOpacity(0.4)))),
      margin: const EdgeInsets.only(bottom: 8),
    );
  }
}
