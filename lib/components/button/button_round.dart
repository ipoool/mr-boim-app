import 'package:flutter/material.dart';

class ButtonRound extends StatelessWidget {
  final String label;
  final IconData? icon;
  final VoidCallback? onTap;
  const ButtonRound({super.key, required this.label, this.icon, this.onTap});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      splashColor: Colors.transparent,
      onTap: onTap,
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 8),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(50),
            border: Border.all(color: Colors.grey.withOpacity(0.4))),
        child: Row(
          children: [
            if (icon != null)
              Icon(
                icon,
                size: 20,
                color: Colors.black87,
              ),
            if (icon != null) const SizedBox(width: 2),
            Text(label,
                style: const TextStyle(
                    fontWeight: FontWeight.bold, color: Colors.black87))
          ],
        ),
      ),
    );
  }
}
