import 'package:flutter/material.dart';
import 'package:mrboim_app/helpers/format.dart';

class ButtonOrderWash extends StatelessWidget {
  final String activeWashType;
  final int total;
  final double totalPrice;
  final VoidCallback? onTap;
  const ButtonOrderWash(
      {super.key,
      required this.activeWashType,
      required this.total,
      required this.totalPrice,
      this.onTap});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      height: 60,
      child: InkWell(
        onTap: onTap,
        borderRadius: BorderRadius.circular(100),
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 28),
          decoration: BoxDecoration(
              color: const Color(0xff00AA13),
              borderRadius: BorderRadius.circular(100)),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                      activeWashType == "satuan"
                          ? "${total.toString()} item"
                          : "${total.toString()} kilogram",
                      style: const TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.bold)),
                  const Text("Pesan sekarang yuk!",
                      style: TextStyle(color: Colors.white))
                ],
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(formatRupiah(totalPrice),
                      style: const TextStyle(
                          fontSize: 18,
                          color: Colors.white,
                          fontWeight: FontWeight.bold)),
                  const SizedBox(width: 8),
                  const Icon(Icons.shopping_basket, color: Colors.white)
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
