import 'package:flutter/material.dart';
import 'package:mrboim_app/commons/app.dart';

class ButtonDeliveryDay extends StatelessWidget {
  final String title;
  final String subtitle;
  final String description;
  final VoidCallback? onTap;
  final bool isActive;
  const ButtonDeliveryDay(
      {super.key,
      required this.title,
      this.subtitle = '',
      this.description = '',
      this.onTap,
      this.isActive = false});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        padding: const EdgeInsets.all(8),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          border: Border.all(
              color: isActive
                  ? const Color(blueButtonActionColor)
                  : Colors.grey.withOpacity(0.2)),
          color: isActive
              ? const Color(blueButtonActionColor)
              : Colors.grey.withOpacity(0.2),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(title,
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: isActive ? Colors.white : Colors.black)),
                if (subtitle != '')
                  Text(subtitle,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: isActive ? Colors.white : Colors.black)),
              ],
            ),
            if (description != '') const SizedBox(height: 10),
            if (description != '')
              Text(description,
                  style: TextStyle(
                      fontSize: 12,
                      color: isActive ? Colors.white : Colors.black))
          ],
        ),
      ),
    );
  }
}
