import 'package:get/get.dart';
import 'package:mrboim_app/helpers/date_time.dart';

class ScheduleController extends GetxController {
  final RxBool _isSetupSchedule = false.obs;
  RxBool get isSetupSchedule => _isSetupSchedule;

  final Rx<DateTime> _dateTime = dateTimeNow().obs;
  Rx<DateTime> get dateTime => _dateTime;

  void changeDate(DateTime selectedDateTime) {
    _dateTime.value = DateTime(selectedDateTime.year, selectedDateTime.month,
        selectedDateTime.day, selectedDateTime.hour, selectedDateTime.minute);

    _isSetupSchedule.value = true;
  }
}
