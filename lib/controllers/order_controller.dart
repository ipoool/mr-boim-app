import 'package:get/get.dart';
import 'package:mrboim_app/commons/app.dart';

class OrderController extends GetxController {
  final RxInt _totalQty = 0.obs;
  RxInt get totalQty => _totalQty;

  final RxString _washType = "satuan".obs;
  RxString get washType => _washType;

  final RxInt _totalKilo = defaultTotalKilo.obs;
  RxInt get totalKilo => _totalKilo;

  final RxMap<String, int> _listCart = <String, int>{}.obs;
  RxMap get listCart => _listCart;

  calculateTotalQty() {
    int total = 0;
    _listCart.forEach((key, value) {
      total += value;
    });
    _totalQty.value = total;
  }

  void increaseQty(String item) {
    if (_listCart[item] == null) {
      _listCart[item] = 1;
    } else {
      _listCart.update(item, (value) => value + 1);
    }
    calculateTotalQty();
  }

  void decreaseQty(String item) {
    if (_listCart[item] == null) {
      _listCart[item] = 0;
    } else {
      _listCart.update(item, (value) => value - 1);
    }
    calculateTotalQty();
  }

  void changeTotalKilo(int kilo) {
    _totalKilo.value = kilo;
  }

  void clearTotalQty() {
    _listCart.clear();
    _totalQty.value = 0;
  }

  void clearTotalKilo() {
    _totalKilo.value = 0;
  }

  void changeWashType(String value) {
    _washType.value = value;
  }
}
