import 'package:easy_date_timeline/easy_date_timeline.dart';
import 'package:flutter/material.dart';
import 'package:mrboim_app/commons/app.dart';
import 'package:mrboim_app/components/bottom_sheet.dart';
import 'package:mrboim_app/components/time_list.dart';
import 'package:mrboim_app/controllers/schedule_controller.dart';
import 'package:mrboim_app/helpers/date_time.dart';

class ScheduleDateTimeBottomSheet extends StatefulWidget {
  final ScheduleController controller;
  const ScheduleDateTimeBottomSheet({super.key, required this.controller});

  @override
  State<ScheduleDateTimeBottomSheet> createState() =>
      _ScheduleDateTimeBottomSheetState();
}

class _ScheduleDateTimeBottomSheetState
    extends State<ScheduleDateTimeBottomSheet> {
  DateTime selectedDate = DateTime.now();
  TimeOfDay selectedTime =
      TimeOfDay(hour: DateTime.now().hour, minute: DateTime.now().minute);

  @override
  void initState() {
    setState(() {
      selectedTime = TimeOfDay(
          hour: widget.controller.dateTime.value.hour,
          minute: widget.controller.dateTime.value.minute);
    });
    super.initState();
  }

  void onChangeDate(DateTime value) {
    widget.controller.dateTime(value);
    setState(() {
      selectedDate = value;
    });
  }

  void onChageTime(TimeOfDay time) {
    setState(() {
      selectedTime = time;
    });
  }

  @override
  Widget build(BuildContext context) {
    void onConfirmShedule() {
      DateTime dateTime = DateTime(
          selectedDate.year,
          selectedDate.month,
          selectedDate.day,
          selectedTime!.hour,
          selectedTime!.minute,
          selectedDate.second);
      widget.controller.changeDate(dateTime);

      Navigator.pop(context);
    }

    return BottomSheetComponent(
      height: 437,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text("Jadwal penjemputan",
              style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold)),
          const SizedBox(height: 16),
          Container(
            margin: const EdgeInsets.only(top: 16, bottom: 16),
            child: EasyDateTimeLine(
              initialDate: widget.controller.dateTime.value,
              onDateChange: onChangeDate,
              headerProps: const EasyHeaderProps(showHeader: false),
              disabledDates: getListDateBeforeDateNow(),
              dayProps: const EasyDayProps(
                dayStructure: DayStructure.dayStrDayNumMonth,
                activeDayStyle: DayStyle(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    color: Color(blueButtonActionColor),
                  ),
                ),
              ),
            ),
          ),
          const SizedBox(height: 16),
          const Text(
            "Jam Jemput",
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          Container(
            margin: const EdgeInsets.only(top: 16),
            child: TimeList(
              onChange: onChageTime,
              currentTime: selectedTime,
              dateTime:
                  widget.controller.dateTime.value.day == DateTime.now().day
                      ? DateTime.now()
                      : DateTime(
                          widget.controller.dateTime.value.year,
                          widget.controller.dateTime.value.month,
                          widget.controller.dateTime.value.day),
            ),
          ),
          const SizedBox(height: 32),
          Container(
            width: double.infinity,
            child: InkWell(
              onTap: selectedTime != null ? onConfirmShedule : () {},
              child: Container(
                padding: const EdgeInsets.all(16),
                decoration: BoxDecoration(
                  color: selectedTime != null
                      ? const Color(0xff00AA13)
                      : Colors.grey.withOpacity(0.4),
                  borderRadius: BorderRadius.circular(100),
                ),
                child: Center(
                  child: Text("Terapkan sekarang",
                      style: TextStyle(
                        fontSize: 16,
                        color: selectedTime != null
                            ? Colors.white
                            : Colors.black45,
                        fontWeight: FontWeight.bold,
                      )),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
