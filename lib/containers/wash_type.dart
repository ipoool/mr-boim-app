import 'package:flutter/material.dart';
import 'package:mrboim_app/commons/app.dart';
import 'package:mrboim_app/commons/constant.dart';
import 'package:mrboim_app/components/alert.dart';

class WashType extends StatelessWidget {
  final String active;
  final VoidCallback? onTap;
  const WashType({super.key, required this.active, this.onTap});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        children: [
          if (active == "satuan")
            const Alert(
                icon: Icons.info_rounded,
                type: AlertType.info,
                label: "Cucian numpuk tapi mau kiloan? Klik \"Ganti\""),
          if (active == "satuan") const SizedBox(height: 10),
          Container(
            padding: const EdgeInsets.all(16),
            decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.grey.withOpacity(0.4),
                ),
                color: Colors.white,
                borderRadius: BorderRadius.circular(24)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    SizedBox(
                      width: 40,
                      child: active == "satuan"
                          ? Image.asset(washTypeList["satuan"]["image"])
                          : Image.asset(washTypeList["kilo"]["image"]),
                    ),
                    const SizedBox(width: 16),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                            active == "satuan"
                                ? washTypeList["satuan"]["title"]
                                : washTypeList["kilo"]["title"],
                            style: const TextStyle(
                                fontSize: 16, fontWeight: FontWeight.bold)),
                        Text(
                            active == "satuan"
                                ? washTypeList["satuan"]["subtitle"]
                                : washTypeList["kilo"]["subtitle"],
                            style: const TextStyle(color: Colors.grey))
                      ],
                    )
                  ],
                ),
                InkWell(
                  onTap: onTap,
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(100),
                        border: Border.all(
                          color: const Color(blueButtonActionColor),
                        )),
                    padding: const EdgeInsets.symmetric(
                        horizontal: 16, vertical: 10),
                    child: const Text("Ganti",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Color(blueButtonActionColor))),
                  ),
                )
              ],
            ),
          ),
          const SizedBox(height: 16),
        ],
      ),
    );
  }
}
