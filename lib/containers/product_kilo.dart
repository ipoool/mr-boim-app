import 'package:flutter/material.dart';
import 'package:mrboim_app/commons/app.dart';
import 'package:mrboim_app/controllers/order_controller.dart';

class ProductKilo extends StatefulWidget {
  final OrderController controller;
  const ProductKilo({super.key, required this.controller});

  @override
  State<ProductKilo> createState() => _ProductKiloState();
}

class _ProductKiloState extends State<ProductKilo> {
  double currentValue = defaultTotalKilo.toDouble();

  void onChangeSlider(double value) {
    setState(() {
      currentValue = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(height: 16),
          const Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text("Est. Kiloan",
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
              SizedBox(width: 6),
              Tooltip(
                message: "Berat cucian akan ditimbang ulang",
                child: Icon(
                  Icons.info_rounded,
                  size: 20,
                  color: Colors.grey,
                ),
              )
            ],
          ),
          const SizedBox(height: 16),
          Slider(
              value: currentValue,
              min: 1,
              max: defaultLimitKilo,
              divisions: 18,
              activeColor: const Color(blueButtonActionColor),
              label: "${currentValue.round().toString()} kg",
              onChanged: (double value) {
                widget.controller.changeTotalKilo(value.toInt());
                onChangeSlider(value);
              }),
          const SizedBox(height: 16),
          Divider(color: Colors.grey.withOpacity(0.4))
        ],
      ),
    );
  }
}
