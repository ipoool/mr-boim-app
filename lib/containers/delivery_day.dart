import 'package:flutter/material.dart';
import 'package:mrboim_app/components/button/button_delivery_day.dart';

class DeliveryDay extends StatelessWidget {
  final String active;
  final ValueChanged<String> onChange;
  const DeliveryDay(
      {super.key, this.active = 'reguler', required this.onChange});

  @override
  Widget build(BuildContext context) {
    return GridView.count(
      physics: const NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      crossAxisCount: 2,
      crossAxisSpacing: 16,
      mainAxisSpacing: 16,
      children: [
        ButtonDeliveryDay(
          title: "Reguler",
          subtitle: "(1-2 hari)",
          description: "No Extra Fee",
          onTap: () {
            onChange('reguler');
          },
          isActive: active == 'reguler',
        ),
        ButtonDeliveryDay(
          title: "Express 4 jam",
          subtitle: "Min. 2kg",
          description: "Harga 4x dari layanan",
          onTap: () {
            onChange('express_4hour');
          },
          isActive: active == 'express_4hour',
        ),
        ButtonDeliveryDay(
          title: "Express 8 jam",
          subtitle: "Min. 2kg",
          description: "Harga 3x dari layanan",
          onTap: () {
            onChange('express_8hour');
          },
          isActive: active == 'express_8hour',
        ),
        ButtonDeliveryDay(
          title: "Express 24 jam",
          subtitle: "Min. 2kg",
          description: "Harga 2x dari layanan",
          onTap: () {
            onChange('ultra_express');
          },
          isActive: active == 'ultra_express',
        )
      ],
    );
  }
}
