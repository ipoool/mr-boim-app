import 'package:flutter/material.dart';
import 'package:mrboim_app/components/alert.dart';
import 'package:mrboim_app/components/bottom_sheet.dart';
import 'package:mrboim_app/components/extra_item.dart';

class ExtraProductBottomSheet extends StatelessWidget {
  const ExtraProductBottomSheet({super.key});

  @override
  Widget build(BuildContext context) {
    return BottomSheetComponent(
      height: 460,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text("Layanan Ekstra Kilat",
              style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold)),
          const SizedBox(height: 16),
          const Alert(
            type: AlertType.success,
            icon: Icons.timer,
            label: "Diburu waktu sampe gak sempet nunggu?",
          ),
          const SizedBox(height: 16),
          const ExtraItem(
            title: "Express 4 jam",
            subtitle: "Harga 4 kali biaya layanan dan min. 2kg",
          ),
          const ExtraItem(
            title: "Express 8 jam",
            subtitle: "Harga 3 kali biaya layanan dan min. 2kg",
          ),
          const ExtraItem(
            title: "Express 24 jam",
            subtitle: "Harga 2 kali biaya layanan dan min. 2kg",
          ),
          const SizedBox(height: 16),
          InkWell(
            onTap: () {},
            child: Container(
              padding: const EdgeInsets.all(16),
              decoration: BoxDecoration(
                color: const Color(0xff00AA13),
                borderRadius: BorderRadius.circular(100),
              ),
              child: const Center(
                child: Text("Terapkan sekarang",
                    style: TextStyle(
                      fontSize: 16,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    )),
              ),
            ),
          )
        ],
      ),
    );
  }
}
