import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mrboim_app/commons/app.dart';
import 'package:mrboim_app/commons/constant.dart';
import 'package:mrboim_app/helpers/date_time.dart';

class ScheduleOption extends StatelessWidget {
  final VoidCallback? onTap;
  final DateTime currentDateTime;
  final bool isSetupSchedule;
  const ScheduleOption(
      {super.key,
      this.onTap,
      this.isSetupSchedule = false,
      required this.currentDateTime});

  @override
  Widget build(BuildContext context) {
    String label;
    if (currentDateTime.day == dateTimeNow().day) {
      label = "Hari ini";
    } else if (currentDateTime.day == dateTimeNow().day + 1) {
      label = "Besok";
    } else {
      label =
          "${dayNameID[currentDateTime.weekday]}, ${currentDateTime.day} ${monthNameIDmin[currentDateTime.month]}";
    }
    label += " jam ${DateFormat("h:mm a").format(currentDateTime)}";

    return Container(
      padding: const EdgeInsets.all(16),
      decoration: BoxDecoration(
          border: Border.all(
            color: Colors.grey.withOpacity(0.4),
          ),
          color: Colors.white,
          borderRadius: BorderRadius.circular(24)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              SizedBox(
                width: 40,
                child: Image.asset("images/date-time.png"),
              ),
              const SizedBox(width: 16),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text("Jadwal penjemputan",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
                  Text(isSetupSchedule ? label : "Atur waktu penjemputan",
                      style: const TextStyle(color: Colors.grey))
                ],
              )
            ],
          ),
          InkWell(
            onTap: onTap,
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(100),
                  border: Border.all(
                    color: const Color(blueButtonActionColor),
                  )),
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
              child: const Text("Ganti",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Color(blueButtonActionColor))),
            ),
          )
        ],
      ),
    );
  }
}
