import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mrboim_app/commons/constant.dart';
import 'package:mrboim_app/components/bottom_sheet.dart';
import 'package:mrboim_app/components/card/card_product_item.dart';
import 'package:mrboim_app/components/service_type_option.dart';
import 'package:mrboim_app/controllers/order_controller.dart';

class ProductPiece extends StatefulWidget {
  final OrderController controller;
  const ProductPiece({super.key, required this.controller});

  @override
  State<ProductPiece> createState() => _ProductPieceState();
}

class _ProductPieceState extends State<ProductPiece> {
  String activeDeliveryDay = 'reguler';

  void onChangeDeliveryDay(String deliveryDay) {
    setState(() {
      activeDeliveryDay = deliveryDay;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Obx(() => Column(
            children: [
              ...listItems.map((item) => CardProductItem(
                    title: item.title,
                    price: 23800,
                    image: item.image,
                    qty: (widget.controller.listCart[item.key] == null)
                        ? 0
                        : widget.controller.listCart[item.key],
                    removePressed: () {
                      widget.controller.decreaseQty(item.key);
                    },
                    addPressed: () {
                      widget.controller.increaseQty(item.key);
                    },
                    serviceTypePressed: () {
                      showModalBottomSheet(
                          context: context,
                          backgroundColor: Colors.white,
                          builder: (context) => BottomSheetComponent(
                              height: 310,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  const Text("Kaos",
                                      style: TextStyle(
                                          fontSize: 22,
                                          fontWeight: FontWeight.bold)),
                                  const SizedBox(height: 16),
                                  const ServiceTypeOption(
                                    title: "Cuci aja",
                                    price: 23800,
                                  ),
                                  const ServiceTypeOption(
                                    title: "Gosok aja",
                                    price: 12400,
                                  ),
                                  const ServiceTypeOption(
                                    title: "Cuci dan Gosok",
                                    price: 32000,
                                  ),
                                  const SizedBox(height: 16),
                                  InkWell(
                                    onTap: () {},
                                    child: Container(
                                      padding: const EdgeInsets.all(16),
                                      decoration: BoxDecoration(
                                        color: const Color(0xff00AA13),
                                        borderRadius:
                                            BorderRadius.circular(100),
                                      ),
                                      child: const Center(
                                        child: Text("Terapkan sekarang",
                                            style: TextStyle(
                                              fontSize: 16,
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold,
                                            )),
                                      ),
                                    ),
                                  )
                                ],
                              )));
                    },
                  ))
            ],
          )),
    );
  }
}
