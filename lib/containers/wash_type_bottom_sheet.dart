import 'package:flutter/material.dart';
import 'package:mrboim_app/commons/constant.dart';
import 'package:mrboim_app/components/bottom_sheet.dart';
import 'package:mrboim_app/components/wash_type_item.dart';
import 'package:mrboim_app/controllers/order_controller.dart';

class WashTypeBottomSheet extends StatelessWidget {
  final OrderController controller;
  const WashTypeBottomSheet({super.key, required this.controller});

  @override
  Widget build(BuildContext context) {
    return BottomSheetComponent(
        height: 314,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text("Pilih jenis cuci",
                style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold)),
            const SizedBox(height: 16),
            WashTypeItem(
              image: washTypeList["satuan"]["image"],
              title: washTypeList["satuan"]["title"],
              subtitle: washTypeList["satuan"]["subtitle"],
              isActive: controller.washType.value == "satuan",
              onTap: () {
                controller.changeWashType("satuan");
                controller.clearTotalKilo();
                Navigator.pop(context);
              },
            ),
            const SizedBox(height: 16),
            WashTypeItem(
              image: washTypeList["kilo"]["image"],
              title: washTypeList["kilo"]["title"],
              subtitle: washTypeList["kilo"]["subtitle"],
              isActive: controller.washType.value == "kilo",
              onTap: () {
                controller.changeWashType("kilo");
                controller.clearTotalQty();
                Navigator.pop(context);
              },
            ),
          ],
        ));
  }
}
