import 'package:flutter/material.dart';
import 'package:mrboim_app/commons/app.dart';

class LocationBottomSheet extends StatefulWidget {
  const LocationBottomSheet({super.key});

  @override
  State<LocationBottomSheet> createState() => _LocationBottomSheetState();
}

const double minSheetSize = 0.2;
const double maxSheetSize = 0.4;

class _LocationBottomSheetState extends State<LocationBottomSheet> {
  final key = GlobalKey();
  final controller = DraggableScrollableController();
  bool isShowForm = false;

  @override
  void initState() {
    super.initState();
    controller.addListener(onChange);
  }

  void onChange() {
    final currentSize = controller.size;
    if (currentSize <= 0.05) _collapse();
    if (currentSize == maxSheetSize) {
      setState(() {
        isShowForm = true;
      });
    } else {
      setState(() {
        isShowForm = false;
      });
    }
  }

  void _collapse() => _animateSheet(sheet.snapSizes!.first);

  void _anchor() => _animateSheet(sheet.snapSizes!.last);

  void _expand() => _animateSheet(sheet.maxChildSize);

  void _hide() => _animateSheet(sheet.minChildSize);

  void _animateSheet(double size) {
    controller.animateTo(
      size,
      duration: const Duration(milliseconds: 50),
      curve: Curves.easeInOut,
    );
  }

  @override
  void dispose() {
    super.dispose();
    controller.dispose();
  }

  DraggableScrollableSheet get sheet =>
      (key.currentWidget as DraggableScrollableSheet);

  @override
  Widget build(BuildContext context) {
    return DraggableScrollableSheet(
        key: key,
        initialChildSize: minSheetSize,
        maxChildSize: maxSheetSize,
        minChildSize: 0,
        expand: false,
        snap: true,
        snapSizes: const [minSheetSize],
        controller: controller,
        builder: (BuildContext context, ScrollController scrollController) {
          return DecoratedBox(
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                    color: Colors.grey.withOpacity(0.2),
                    blurRadius: 10,
                    spreadRadius: 3)
              ],
              color: Colors.white,
              borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(16),
                topRight: Radius.circular(16),
              ),
            ),
            child: CustomScrollView(
              controller: scrollController,
              slivers: [
                SliverToBoxAdapter(
                  child: Stack(
                    alignment: AlignmentDirectional.topCenter,
                    children: [
                      Container(
                        decoration: BoxDecoration(
                            color: Colors.grey.withOpacity(0.4),
                            borderRadius: BorderRadius.circular(100)),
                        width: 30,
                        height: 5,
                        margin: const EdgeInsets.only(top: 10, bottom: 16),
                      )
                    ],
                  ),
                ),
                SliverList.list(
                  children: [
                    Container(
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: const Text("Lokasi",
                          style: TextStyle(
                              fontSize: 22, fontWeight: FontWeight.bold)),
                    ),
                    const SizedBox(height: 16),
                    Container(
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: Row(
                        children: [
                          const Icon(
                            Icons.fmd_good_rounded,
                            color: Colors.pinkAccent,
                            size: 28,
                          ),
                          const SizedBox(width: 10),
                          const Flexible(
                            child: Text(
                              "Jalan mandor basar no 104, Rangkapan jaya, Pancoranmas, Depok",
                            ),
                          ),
                        ],
                      ),
                    ),
                    Visibility(
                        // visible: isShowForm,
                        child: Column(
                      children: [
                        Container(
                          padding: const EdgeInsets.symmetric(horizontal: 16),
                          child: TextFormField(
                            maxLines: 2,
                            decoration: const InputDecoration(
                              labelText: "Detail Alamat",
                              helperText:
                                  "Kamu bisa kasih tau patokan arah rumah",
                            ),
                          ),
                        ),
                        const SizedBox(height: 32),
                        Container(
                          padding: const EdgeInsets.symmetric(horizontal: 16),
                          child: InkWell(
                            onTap: () => {},
                            child: Container(
                              width: double.infinity,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(100),
                                  color: const Color(blueButtonActionColor),
                                  border: Border.all(
                                    color: const Color(blueButtonActionColor),
                                  )),
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 16, vertical: 16),
                              child: const Center(
                                child: Text("Konfirmasi",
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white)),
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(height: 16),
                      ],
                    ))
                  ],
                ),
              ],
            ),
          );
        });
  }
}
